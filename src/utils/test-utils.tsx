import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../redux/store';


const AllTheProviders = (props:any) => {
  return (
    <Provider store={store}>
        {props.children}
     </Provider>
  )
}

const customRender = (ui: any, options: any) => {
  return render(ui, { wrapper: AllTheProviders, ...options })
}

export * from '@testing-library/react'
export {customRender as render}