import styles from  './App.module.scss';
import { useAppDispatch, useAppSelector } from './redux/hooks';
import { citiesSlice } from './redux/slices/citySlice/citySlice';
import { Form } from './components/form/Form'
import { List } from './components/list/List';
import { ErrorAlert } from './components/errorAlert/errorAlert';


function App() {


  return (
    <div className={styles.App}>
      <ErrorAlert/>
      <div className={styles.container}>
        <Form />
        <List />
      </div>
    </div>
  );
}

export default App;
