import { put, takeEvery } from "redux-saga/effects";
import { watchFetchCityWeather, fetchCityWeather } from "../citySliceSagas"

const mockResp = {
	"data": {
		"coord": { "lon": 19.4667, "lat": 51.75 },
		"weather": [{ "id": 803, "main": "Clouds", "description": "broken clouds", "icon": "04n" }],
		"base": "stations", "main": { "temp": 17.52, "feels_like": 17.34, "temp_min": 15.59, "temp_max": 17.9, "pressure": 1014, "humidity": 77 }, "visibility": 10000,
		"wind": { "speed": 3.09, "deg": 260 }, "clouds": { "all": 75 }, "dt": 1629404181,
		"sys": { "type": 1, "id": 1706, "country": "PL", "sunrise": 1629344014, "sunset": 1629395896 },
		"timezone": 7200, "id": 3093133, "name": "Łódź", "cod": 200
	}
}

describe('watchFetchCityWeather', () => {
	const genObj = watchFetchCityWeather();
	it('wait for cities/addCityToList', () => {
		expect(genObj.next().value).toEqual(takeEvery("ADD_CITY", fetchCityWeather))
	});



	it('iteration end', () => {
		expect(genObj.next().done).toBeTruthy();
	});




});

