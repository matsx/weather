import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../../store';

const basicData:CitiesSlice['citiesList'] = [
	{
		name: "Beijing",
	},
	{
		name: "Bangkok",
	},
	{
		name: "Los Angeles",
	}
]

export interface City {
	name: string;
	weatherData?: object;
	refreshTime?: Date | number
}

interface CitiesSlice {
	selectedCity: string;
	citiesList: City[];
	cityWeatherError: string
}

const initialState: CitiesSlice = {
	selectedCity: '',
	citiesList: basicData,
	cityWeatherError: ''
}



export const citiesSlice = createSlice({
	name: 'cities',
	initialState,
	reducers: {
		setSelectedCity: (state, action: PayloadAction<string>) => {
			state.selectedCity = action.payload
		},
		addCityToList: (state, action: PayloadAction<City>) => {
			if (!state.citiesList.some(el => el.name === action.payload.name)) {
				state.citiesList.push(action.payload)
			}
			else {
				state.citiesList = state.citiesList.map(cityData => cityData.name === action.payload.name ? action.payload : cityData)
			}
		},
		cityWeatherError: (state, action: PayloadAction<string>) => {
			state.cityWeatherError = action.payload
		},
		removeCityFromList: (state, action: PayloadAction<string>) => {
			state.citiesList = state.citiesList.filter((el) => el.name !== action.payload)
		}
	},
})


// export const selectCity = (state: RootState) => state.counter.value

export default citiesSlice.reducer