import { PayloadAction } from '@reduxjs/toolkit';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { call, put, takeEvery, takeLeading } from 'redux-saga/effects';
import { citiesSlice } from './citySlice';

export const middlewareAction= "ADD_CITY"


export function* fetchCityWeather(action: PayloadAction<string>): Generator<any, any> {
	try {
		const resp = yield call(
			axios.get,
			`https://api.openweathermap.org/data/2.5/weather?q=${action.payload}&appid=048bedafbebad2d3f8b97f4b7b359fdc&units=metric`
		)
		if (resp) {
			const responseData = resp as AxiosResponse<any>;
			if (responseData.data.cod === 200) {
				const { main, weather, wind } = responseData.data
				yield put(citiesSlice.actions.addCityToList({
					name: action.payload,
					weatherData: { main, weather, wind },
					refreshTime: Date.now()
				}))
			}
			else {
				yield put(citiesSlice.actions.cityWeatherError(responseData.data?.msg || "Error occured during fetching a weather"))
			}
		}
	}
	catch (error) {
		if ((error as any).isAxiosError){
			const axiosError = <AxiosError>error;
			yield put(citiesSlice.actions.cityWeatherError(axiosError.message || "Error occured during fetching a weather"))
		}
		else {
			yield put(citiesSlice.actions.cityWeatherError("Error occured during fetching a weather"))
		}
	}
};

export function* watchFetchCityWeather() {
	yield takeEvery(middlewareAction, fetchCityWeather )
}