import { all } from 'redux-saga/effects'
import { watchFetchCityWeather } from './slices/citySlice/citySliceSagas'


export default function* rootSaga() {
	yield all([
		watchFetchCityWeather()
	])
}