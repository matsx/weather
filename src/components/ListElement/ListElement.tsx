import style from './ListElement.module.scss';
import { citiesSlice, City } from '../../redux/slices/citySlice/citySlice'
import { useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import { WeatherDetails } from '../WeatherDetails/WeatherDetails';
import { middlewareAction } from '../../redux/slices/citySlice/citySliceSagas';

interface Props {
	data: City
}

export const ListElement = (props: Props) => {
	const [focused, toggleFocus] = useState(false);
	const { name, weatherData, refreshTime } = props.data;
	const dispatch = useAppDispatch();
	const selectedCity = useAppSelector(store => store.citiesSlice.selectedCity)

	const toggleData = () => {
		if (name === selectedCity) {
			return dispatch(citiesSlice.actions.setSelectedCity(''));
		}
		dispatch(citiesSlice.actions.setSelectedCity(name))
		if (!refreshTime) {
				dispatch({ type: middlewareAction, payload: name });
			}
	}

	const removeElement = () => {
		if (name === selectedCity) {
			dispatch(citiesSlice.actions.setSelectedCity(''));

		};
		dispatch(citiesSlice.actions.removeCityFromList(name));

	}

	return (
		<li className={style.listElement}
			onMouseEnter={() => toggleFocus(true)}
			onMouseLeave={() => toggleFocus(false)}>
			<div className={style.container}>
				<div className={style.cityNameContainer}>
					<span className={style.cityName}>{name}</span>
				</div>
			{focused ?
				<div className={style.actionsContainer}>
					<button className={style.showDataButton} onClick={toggleData}>{selectedCity !== name ? 'Show data' : 'Hide data'}</button>
					<button className={style.removeCityButton} onClick={removeElement}>Remove</button>
				</div>
					: <span className={style.basicInfo}>{weatherData ? (weatherData as any)?.main?.temp +  '°C' : ''}</span>}
			</div>
			{name === selectedCity && refreshTime ? <WeatherDetails weatherData={weatherData} refreshTime={refreshTime}/> : null}
		</li>
	)
}