import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import style from './WeatherDetails.module.scss';
import background from './background.jpg';
import citySlice, { citiesSlice, City } from '../../redux/slices/citySlice/citySlice';
import { useEffect } from "react";
import { middlewareAction } from "../../redux/slices/citySlice/citySliceSagas";
import Zoom from 'react-reveal/Zoom';


interface Props {
	weatherData: City['weatherData'];
	refreshTime: City['refreshTime'];
}

export const WeatherDetails = (props:Props) => {
	const selectedCity = useAppSelector(store => store.citiesSlice.selectedCity);
	const { temp, feels_like, pressure, humidity } = (props?.weatherData! as any).main;
	const { description } = (props?.weatherData! as any).weather[0];
	const { speed } = (props?.weatherData! as any).wind;
	const dispatch = useAppDispatch();

	useEffect(() => {
		const interval = setInterval(() => {
			dispatch({ type: middlewareAction, payload: selectedCity })
		}, 10000)
		return () => {
				clearInterval(interval)
			}
	}, [])

	return (
			<Zoom>
			<div className={style.container}>
				<img src={background} alt="shoreImage" className={style.image} />
				<div className={style.gradientEffect}></div>
				<div className={style.weatherData}>
					<p className={style.cityName}>{selectedCity.toUpperCase()}</p>
					<div className={style.detailsContainer}>
						<div>
							<p className={style.weatherDataElement}>Current temp.: {temp} °C</p>
							<p className={style.weatherDataElement}>Sensed temp.: {feels_like} °C</p>
							<p className={style.weatherDataElement}>Pressure: {pressure} hPa</p>
						</div>
						<div>
							<p className={style.weatherDataElement}>Humidity: {humidity} %</p>
							<p className={style.weatherDataElement}>Wind speed: { speed }</p>
							<p className={style.weatherDataElement}>Description:</p>
							<p className={style.weatherDataElement}>{description}</p>
							<p className={style.weatherDate}>Latest refresh: {new Date(props.refreshTime!).toLocaleString()}</p>
						</div>
					</div>

				</div>
			</div>
			</Zoom>
		)
}