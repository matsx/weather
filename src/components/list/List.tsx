import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { ListElement } from "../ListElement/ListElement";
import style from "./List.module.scss"


export const List = () => {
	const cityData = useAppSelector(store => store.citiesSlice.citiesList);
	const renderList = () => {
		const data = cityData.map(city => (
			<ListElement data={city} />
		))
		return data;
	}

	return (
		<div className={style.listContainer}>
			{cityData.length ? <ul className={style.citiesList}>{renderList()}</ul>: <p>Add city to check current weather.</p> }
		</div>
	)
}