import { useState } from 'react'
import { useAppDispatch } from '../../redux/hooks';
import { citiesSlice } from '../../redux/slices/citySlice/citySlice';
import { middlewareAction } from '../../redux/slices/citySlice/citySliceSagas';
import styles from './Form.module.scss';

export const Form = () => {
	const [cityInput, setCityInput] = useState('');
	const dispatch = useAppDispatch();

	const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
		setCityInput(event.currentTarget.value);
	}

	const addToList = () => {
		dispatch({ type: middlewareAction, payload: cityInput });
		setCityInput('');
	}

	return (

			<div className={styles.formContainer}>
			<input type="text" name="city" id="city" className={styles.cityNameInput} onChange={handleChange} value={cityInput}/>
				<button
					className={styles.submitCity}
					disabled={cityInput.length < 3 ? true : false}
					onClick={addToList}
				>Add city</button>
			</div>
	)
}