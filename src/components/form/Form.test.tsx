import {  screen, fireEvent } from '@testing-library/react'
import { Form } from './Form'
import { render } from '../../utils/test-utils';



test("Render a form", async () => {
	const { container, getByRole } = render(<Form />, {});
	expect(getByRole('textbox')).toBeInTheDocument();
	expect(getByRole('button')).toBeInTheDocument();
	expect(getByRole('button')).toBeDisabled();

	let input = await screen.findByRole('textbox');
	fireEvent.change(input, { target: { value: "TestCity" } });
	expect(getByRole('button')).toBeEnabled();



})