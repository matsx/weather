import { screen } from '@testing-library/react';
import { store } from '../../redux/store';
import { ErrorAlert } from './errorAlert';
import { render } from '../../utils/test-utils';


test('Render Alert on fetch error.', async () => {
	store.dispatch({ type: 'cities/cityWeatherError', payload: 'Example error!' })
	const component = render(<ErrorAlert />, {});
	const errorBar = component.queryByText('Example error!');
	expect(errorBar?.textContent).toEqual('Example error!')
});

test('Alert hidden if cities/cityWeatherError is empty', async () => {
	store.dispatch({ type: 'cities/cityWeatherError', payload: '' })
	const Alert = screen.queryAllByTestId('error-alert')
	expect(Alert).toHaveLength(0);
})
