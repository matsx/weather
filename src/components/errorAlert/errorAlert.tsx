import { useAppSelector, useAppDispatch } from "../../redux/hooks"
import { citiesSlice } from "../../redux/slices/citySlice/citySlice";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}




export const ErrorAlert = () => {
	const errorMsg = useAppSelector(store => store.citiesSlice.cityWeatherError);

	const dispatch = useAppDispatch();
	const handleClose = () => {
		dispatch(citiesSlice.actions.cityWeatherError(''));
	}


	return (
		errorMsg ?
		 <Snackbar open={true} autoHideDuration={6000} onClose={handleClose}>
        <Alert data-testid="error-alert" onClose={handleClose} severity="error">
          {errorMsg}
        </Alert>
			</Snackbar>
			: null
	)
}